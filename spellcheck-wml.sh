#!/bin/bash
#
#    spellcheck-wml.sh - Spellchecking script for WML files.
#    Copyright (C) 2020  Rafael Fontenelle <rafaelff [at] gnome [dot] org>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# USAGE
#    - Always run from your webwml language directory
#    - I can:
#          - run without input file to spellcheck WMLs in all subdirectories;
#          - pass one or more translated WML files as input file(s)
# EXAMPLE
#    - [webwml/portuguese] $ spellcheck-wml.sh international/index.wml

################

# List of input files, targets of the spellchecking; if empty, run on every wml
wml_list="$*"

# filename containing custom words to consider as valid when spellchecking
custom_dict=$(dirname "$0")/dict

# the root directory of the misspelling reports; an "out" subdirectory
# stores individual checks, while the root dir stores the compiled of
# all words found without duplicity.
report_file_root_dir=spellcheck

################

HUNSPELL=$(which hunspell) || exit 1

mkdir -p $report_file_root_dir

# if no input file provided, list all WML in the tree of subdirectories
[ -z "$wml_list" ] && wml_list=$(find . -name '*.wml' | sort -u)

# if custom dictionary not found, do not use (not recommended)
use_custom_dict_opt=""
if [ -r "$custom_dict" ]; then
    use_custom_dict_opt="-p $custom_dict"
    echo "Using custom dictionary '$custom_dict'."
else
    echo "No valid custom dictionary, continuing without it."
fi

cur=0
total=$(echo $wml_list | tr ' ' '\n' | wc -l)
tmp_report="$(mktemp)"
for wml in $wml_list; do
    if [ -r "$wml" ]; then
        #input file is a readable file, continue
        wml="${wml##./}"
        report_dir="$report_file_root_dir/out/$(dirname $wml)"
        report_file=$(basename ${wml/.wml/.txt})
        cur=$((cur + 1))

        mkdir -p $report_dir;
        touch $report_dir/$report_file;

        echo -en "[$cur/$total]\t$wml  →  "
        cat $wml | \
        sed \
            -e '/^#/d' \
            -e '/^<!--/d' | \
        $HUNSPELL $use_custom_dict_opt -H -l | \
        $HUNSPELL -d en_US -l \
        > "$tmp_report"
        
        # create & print report file, if temp report file has any content
        if [ -s "$tmp_report" ]; then
            cp "$tmp_report" "$report_dir/$report_file"
            echo "out/$(dirname $wml)/$report_file"
        else
            echo "OK 🗸"
        fi
    else
        echo "'$wml' is not a valid WML file, skipping."
    fi
done

# Gather all reports in one file, without repeating words
if [ -d "$report_file_root_dir/out" ]; then
    report_list=$(find . $report_file_root_dir/out -name '*.txt' | sort -u)
    if [ ! -z "$report_list" ]; then
        tmp_report=$(mktemp)
        cat $report_list | sort -u > $tmp_report
        mv $tmp_report $report_file_root_dir/built_report.txt
    else
        exit 1
    fi
else
    exit 1
fi
